import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_json_paylaod/model/abilities_model.dart';
import 'package:flutter_json_paylaod/widgets/abilities_card.dart';

class AbilitiesPage extends StatefulWidget {
  static final routes = 'abilities_page';

  @override
  _AbilitiesPageState createState() => _AbilitiesPageState();
}

class _AbilitiesPageState extends State<AbilitiesPage> {
  List<AbilitiesModel> _abilitiesList = [], _searchList = [];

  @override
  void initState() {
    super.initState();
    loadJson();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Abilities List'),
      ),
      body: Container(
        child: abilitiesListView(),
      ),
    );
  }

  void loadJson() async {
    List parsedJson =
        json.decode(await rootBundle.loadString('assets/abilities-list.json'));
    setState(() {
      _abilitiesList =
          parsedJson.map((e) => AbilitiesModel.fromJson(e)).toList();
      _searchList = _abilitiesList;
    });
  }

  Widget abilitiesListView() {
    return _abilitiesList.length > 0
        ? Column(
            children: <Widget>[
              Container(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                child: new TextField(
                  decoration: new InputDecoration(
                    hintText: "Search Abilities",
                  ),
                  onChanged: (value) {
                    setState(() {
                      _searchList = _abilitiesList
                          .where((v) => v.name
                              .toLowerCase()
                              .contains(value.toLowerCase()))
                          .toList();
//                      _searchList.forEach((element) {
//                        element.printAbilities();
//                      });
                    });
                  },
                ),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: _searchList.length,
                  itemBuilder: (context, index) {
                    AbilitiesModel item = _searchList[index];
                    return Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: AbilitiesCard(
                        thumbnail: Container(
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(item.logoUrl),
                                fit: BoxFit.contain,
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(50.0)),
                              boxShadow: [
                                BoxShadow(blurRadius: 0.1, color: Colors.black)
                              ]),
                        ),
                        title: item.name,
                        subtitle: item.description,
                      ),
                    );
                  },
                ),
              ),
            ],
          )
        : Center(child: new CircularProgressIndicator());
  }
}
