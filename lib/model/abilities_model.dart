class AbilitiesModel {
  final String name;
  final String description;
  final String logoUrl;

  AbilitiesModel.fromJson(Map<String, dynamic> parsedJson)
      : name = parsedJson['name'].toString(),
        description = parsedJson['description'].toString(),
        logoUrl = parsedJson['logo_url'].toString();

  printAbilities() {
    print('$name: $description');
  }
}
