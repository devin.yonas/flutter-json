import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class AbilitiesCard extends StatelessWidget {
  final Widget thumbnail;
  final String title, subtitle;
  final double titleSize, subtitle1Size;

  AbilitiesCard({
    this.thumbnail,
    @required this.title,
    this.subtitle = '',
    this.titleSize = 22.0,
    this.subtitle1Size = 15.0,
  });

  @override
  Widget build(BuildContext context) {
    Brightness brightness = SchedulerBinding.instance.window.platformBrightness;

    return Card(
      elevation: 5,
      child: Container(
        height: 120,
        padding: const EdgeInsets.all(10.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            thumbnail != null
                ? AspectRatio(
                    aspectRatio: 1.0,
                    child: thumbnail,
                  )
                : SizedBox(),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      '$title',
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: titleSize,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 5),
                    Text(
                      '$subtitle',
                      textAlign: TextAlign.justify,
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: subtitle1Size,
                        color: brightness == Brightness.dark
                            ? Colors.grey.shade400
                            : Colors.grey.shade700,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
