import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_json_paylaod/view/abilites_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    var brightness = SchedulerBinding.instance.window.platformBrightness;

    return MaterialApp(
      title: 'Flutter JSON',
      theme:
          brightness == Brightness.dark ? ThemeData.dark() : ThemeData.light(),
      initialRoute: AbilitiesPage.routes,
      routes: {
        AbilitiesPage.routes: (BuildContext context) => AbilitiesPage(),
      },
    );
  }
}
